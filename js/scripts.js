//CREO ARRAY DE OBJETOS
let arrayPersonajes = [];


// CREO LA CLASE HÉROE
class Heroe {
    constructor(nombre, experiencia, ptosVida, tipo, img, progreso, trucos) {
        this._nombre = nombre;
        this._experiencia = experiencia;
        this._ptosVida = ptosVida;
        this._tipo = tipo;
        this.img = img;
        this.progreso = progreso;
        this._trucos = trucos;
    }

    get nombre() {
        return 'Nombre: ' + this._nombre;
    }

    set tipo(valor) {
        this._tipo = valor;
    }
}

//CREO OBJETOS QUE HEREDAN PROPIEDADES DE LA CLASE HEROE Y A CADA UNO LE DOY UNA PROPIEDAD PROPIA
class Guerrero extends Heroe {
    constructor(nombre, experiencia, ptosVida, tipo, img, progreso, fama, trucos) {
        super (nombre, experiencia, ptosVida, tipo, img, progreso, trucos);
        this._fama = fama;
    }

    get tv() {
        return this._nombre + ' ha ganado ' + this._fama + ' de fama.';
    }

    set cambiaFama (valor) {
        this._fama = valor;
    }

    atacar(enemigo) {
        enemigo._ptosVida -= 50;
        this._experiencia +=20;
    }

}

class Mago extends Heroe {
    constructor(nombre, experiencia, ptosVida, tipo, img, progreso, trucos, magia) {
        super (nombre, experiencia, ptosVida, tipo, img, progreso, trucos);
        this._magia = magia;
    }

    get hechizo() {
        return this._nombre + ' ha gastado su hechizo ' + this._magia;
    }

    atacar(enemigo) {
        enemigo._ptosVida -= 25;
        this._experiencia +=10;
    }
}

class Ladron extends Heroe {
    constructor(nombre, experiencia, ptosVida, tipo, img, progreso, trucos, poder) {
        super (nombre, experiencia, ptosVida, tipo, img, progreso, trucos);
        this._poder = poder;
    }

    robar(enemy) {
        if (true) {
            return this._nombre + ' le ha robado a ' + enemy + ' la cartera y su experiencia ahora es de ' + (this._experiencia+10) + '.';
        }
    }

    atacar(enemigo) {
        enemigo._ptosVida -= 20;
        this._experiencia +=15;
    }

}

//CREO ESOS OBJETOS Y LES LLAMO POR CONSOLA
var guillermo = new Guerrero('Guillermo', 80, 100, 'Guerrero', 'img/guerrero.png', 100, 'Nada', 'Superfuerza');
arrayPersonajes.push(guillermo);

var eduardo = new Mago('Eduardo', 70, 100, 'Mago', 'img/mago.png', 100, 'Invisivilidad', 'Dolor');
arrayPersonajes.push(eduardo);

var cesar = new Ladron('César', 90, 100, 'Ladrón', 'img/ladron.png', 100, 'Carterista', 'Trilero');
arrayPersonajes.push(cesar);


//SELECCIONO LA PLANTILLA
let template = document.querySelector('#item');
let documento = template.content.querySelector('.article');


for(var i = 0; i < arrayPersonajes.length; i++) {
    var nuevoP = documento.cloneNode(true);
    nuevoP.id = "elemDiv" + i;
    let nombre = arrayPersonajes[i]._nombre;
    nuevoP.querySelector(".tipo").innerHTML = arrayPersonajes[i]._tipo;
    nuevoP.querySelector(".nombre").innerHTML = arrayPersonajes[i]._nombre;
    nuevoP.querySelector("progress").value = arrayPersonajes[i].progreso;
    nuevoP.querySelector("img").src = arrayPersonajes[i].img;
    nuevoP.querySelector(".exp").innerHTML = arrayPersonajes[i]._experiencia;
    nuevoP.querySelector(".life").innerHTML = arrayPersonajes[i]._ptosVida;
    nuevoP.querySelector(".trick").innerHTML = arrayPersonajes[i]._trucos;
    let selector = nuevoP.querySelector('select');
    selector.id = 'select'+i;   

    for (let j = 0; j < arrayPersonajes.length; j++){
        if(nombre != arrayPersonajes[j]._nombre){
            addOption(selector, arrayPersonajes[j]._nombre, arrayPersonajes[j]._nombre);
        }
    }

    let botonDiv = nuevoP.querySelector("button");
    botonDiv.id = 'btn'+i;
    botonDiv.addEventListener('click', ataque);
    document.querySelector(".container").appendChild(nuevoP);
    
} 

//CREAR LISTA OPCIONES
function addOption(select, texto, valor) {
    let lista = document.createElement('option');
    lista.text = texto;
    lista.value = valor;

    select.options.add(lista);
}


//DOY FUNCIÓN AL BOTÓN

function ataque(e) {

    let atacante = this.parentNode.querySelector('.nombre').textContent; // ENCUENTRA ATACANTE
    let victima = this.parentNode.querySelector('select').value; // ENCUENTRA VÍCTIMA

    let encontrarAtacante = arrayPersonajes.find((Heroe) => {
        return Heroe._nombre == atacante;
    });

    let encontrarVictima = arrayPersonajes.find((Heroe) => {
        return Heroe._nombre == victima;
    });

    let targetDiv = '#elemDiv'+(arrayPersonajes.indexOf(encontrarVictima));
    let targetAtack = '#elemDiv'+(arrayPersonajes.indexOf(encontrarAtacante));
    let actualizoVida = document.querySelector(targetDiv).querySelector('.propiedades .vida .life');
    let actualizoExp = document.querySelector(targetAtack).querySelector('.propiedades .experiencia .exp');
    let inhabilitoBtn = document.querySelector(targetDiv).querySelector('button');
    let actualizoBarraVida = document.querySelector(targetDiv).querySelector('progress');

    encontrarAtacante.atacar(encontrarVictima);

    let imagen = document.querySelector(targetDiv).querySelector('img');

    if (encontrarVictima._ptosVida > 0) {
        actualizoVida.innerHTML = encontrarVictima._ptosVida;
        actualizoBarraVida.value = encontrarVictima._ptosVida;
        actualizoExp.innerHTML = encontrarAtacante._experiencia;

    } else {
        inhabilitoBtn.disabled = true;
        alert('Ya está muerto');
        actualizoVida.innerHTML = encontrarVictima._ptosVida;
        actualizoBarraVida.value = encontrarVictima._ptosVida;
        imagen.style.filter = 'grayscale(100%)';
    }
 }


